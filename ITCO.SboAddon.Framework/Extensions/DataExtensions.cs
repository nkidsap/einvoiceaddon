﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ITCO.SboAddon.Framework.Extensions
{
    /// <summary>
    /// DataExtension functions
    /// </summary>
    public static class DataExtensions
    {
        /// <summary>
        /// Parse date from 
        /// </summary>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        /// <example>
        /// var date = form.DataSources.UserDataSources.Item("Date").ToDate();
        /// </example>
        public static DateTime? ToDate(this UserDataSource dataSource)
        {
            if (string.IsNullOrEmpty(dataSource.ValueEx))
                return null;

            return DateTime.ParseExact(dataSource.ValueEx, "yyyyMMdd", CultureInfo.InvariantCulture);
        }
        public static System.Data.DataTable SapDataTableToDotNetDataTable(string pathToXmlFile)
        {
            var DT = new System.Data.DataTable();

            var XMLstream = new System.IO.FileStream(pathToXmlFile, FileMode.Open);

            var XDoc = System.Xml.Linq.XDocument.Load(XMLstream);

            var Columns = XDoc.Element("DataTable").Element("Columns").Elements("Column");

            foreach (var Column in Columns)
            {
                DT.Columns.Add(Column.Attribute("Uid").Value);
            }

            var Rows = XDoc.Element("DataTable").Element("Rows").Elements("Row");

            var Names = new List<string>();
            foreach (var Row in Rows)
            {
                var DTRow = DT.NewRow();

                var Cells = Row.Element("Cells").Elements("Cell");
                foreach (var Cell in Cells)
                {
                    var ColName = Cell.Element("ColumnUid").Value;
                    var ColValue = Cell.Element("Value").Value;
                    DTRow[ColName] = ColValue;
                }

                DT.Rows.Add(DTRow);
            }

            return DT;
        }

        
    }
}
