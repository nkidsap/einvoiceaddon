﻿using SAPbouiCOM;
using System;
using ITCO.SboAddon.Framework.Forms;
using ITCO.SboAddon.Framework.Helpers;
using ITCO.SboAddon.Framework.Data;
using System.Collections.Generic;

namespace ITCO.SboAddon.Framework.Extensions
{
    /// <summary>
    /// Form Extensions
    /// </summary>
    public static class FormExtensions
    {
        /// <summary>
        /// Get Form object from SBOItemEventArg
        /// </summary>
        /// <param name="pVal"></param>
        /// <returns>Form object</returns>
        public static Form GetForm(this SBOItemEventArg pVal)
        {
            return SboApp.Application.Forms.Item(pVal.FormUID);
        }

        /// <summary>
        /// Get Item Object in Form
        /// </summary>
        /// <typeparam name="T">Form Item Type</typeparam>
        /// <param name="form"></param>
        /// <param name="itemId">Item Id</param>
        /// <returns></returns>
        [Obsolete("Use GetEditText, GetButton, ... instead")]
        public static T Get<T>(this IForm form, string itemId)
        {
            return (T)form.Items.Item(itemId).Specific;
        }

        /// <summary>
        /// Get Static Text
        /// </summary>
        /// <param name="form"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static StaticText GetStaticText(this IForm form, string itemId)
        {
            return form.Items.Item(itemId).Specific as StaticText;
        }

        /// <summary>
        /// Get Combo Box
        /// </summary>
        /// <param name="form"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static ComboBox GetComboBox(this IForm form, string itemId)
        {
            return form.Items.Item(itemId).Specific as ComboBox;
        }

        /// <summary>
        /// Get Button
        /// </summary>
        /// <param name="form"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static Button GetButton(this IForm form, string itemId)
        {
            return form.Items.Item(itemId).Specific as Button;
        }

        /// <summary>
        /// Get Edit Text
        /// </summary>
        /// <param name="form"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static EditText GetEditText(this IForm form, string itemId)
        {
            return form.Items.Item(itemId).Specific as EditText;
        }

        /// <summary>
        /// Get Check Box
        /// </summary>
        /// <param name="form"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static CheckBox GetCheckBox(this IForm form, string itemId)
        {
            return form.Items.Item(itemId).Specific as CheckBox;
        }

        /// <summary>
        /// Get Matrix
        /// </summary>
        /// <param name="form"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static Matrix GetMatrix(this IForm form, string itemId)
        {
            return form.Items.Item(itemId).Specific as Matrix;
        }

        /// <summary>
        /// Get Grid
        /// </summary>
        /// <param name="form"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public static Grid GetGrid(this IForm form, string itemId)
        {
            return form.Items.Item(itemId).Specific as Grid;
        }

        /// <summary>
        /// Add values into Combobox from SQL
        /// </summary>
        /// <param name="comboBox"></param>
        /// <param name="sql"></param>
        public static void AddComboBoxValues(this ComboBox comboBox, string sql)
        {
            using (var query = new SboRecordsetQuery(sql))
            {
                foreach (var combo in query.Result)
                {
                    comboBox.ValidValues.Add(combo.Item(0).Value.ToString(), combo.Item(1).Value.ToString());
                }
            }
        }
        public static void AddComboBoxValue_Dapper(this ComboBox comboBox, string sql)
        {
            foreach(var obj in new SboDbConnection().Query<dynamic>(sql))
            {
                comboBox.ValidValues.Add(obj.Code.ToString(), obj.Name.ToString());
            }
                
            //using (var query = new SboDbConnection().Query<dynamic>(sql))
            //{
            //    foreach (var combo in query.Result)
            //    {
            //        comboBox.ValidValues.Add(combo.Item(0).Value.ToString(), combo.Item(1).Value.ToString());
            //    }
            //}
        }
        public static void AddButton(this IForm form,string parentId, string itemId,string caption)
        {
            try
            {
                if(parentId!=null)
                {
                    SAPbouiCOM.Item oItem = (Item)form.Items.Item(parentId); /// Existing Item on the form

                    SAPbouiCOM.Item oItem1 = (Item)form.Items.Add(itemId, SAPbouiCOM.BoFormItemTypes.it_BUTTON);

                    oItem1.Top = oItem.Top;

                    oItem1.Left = oItem.Left + oItem.Width + 5;

                    oItem1.Width = oItem.Width + 20;

                    oItem1.Height = oItem.Height;

                    oItem1.Enabled = true;

                    SAPbouiCOM.Button oButton = (SAPbouiCOM.Button)oItem1.Specific;

                    oButton.Caption = caption;
                }    
                
            }
            catch (Exception)
            {

                throw;
            }
           

        }

        public static void AddButtonCombo(this IForm form, string parentId, string itemId,int width = 0)
        {
            try
            {
                if (parentId != null)
                {
                    SAPbouiCOM.Item oItem = (Item)form.Items.Item(parentId); /// Existing Item on the form

                    SAPbouiCOM.Item oItem1 = (Item)form.Items.Add(itemId, SAPbouiCOM.BoFormItemTypes.it_BUTTON_COMBO);

                    oItem1.Top = oItem.Top;

                    oItem1.Left = oItem.Left + oItem.Width + 5;

                    oItem1.Width = width==0? oItem.Width + 20: width;

                    oItem1.Height = oItem.Height;

                    oItem1.Enabled = true;


                    oItem1.DisplayDesc=true;

                    oItem1.AffectsFormMode = false;
                }

            }
            catch (Exception)
            {

                throw;
            }


        }
        public static void AddButtonComboBoxValue_Dapper(this ButtonCombo button, string sql)
        {
            foreach (var obj in new SboDbConnection().Query<dynamic>(sql))
            {
                button.ValidValues.Add(obj.Code.ToString(), obj.Name.ToString());
            }
        }
        public static void AddCombobox(this IForm form, string parentId, string itemId)
        {
            try
            {
                if (parentId != null)
                {
                    SAPbouiCOM.Item oItem = (Item)form.Items.Item(parentId); /// Existing Item on the form

                    SAPbouiCOM.Item oItem1 = (Item)form.Items.Add(itemId, SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);

                    oItem1.Top = oItem.Top;

                    oItem1.Left = oItem.Left + oItem.Width + 5;

                    oItem1.Width = oItem.Width + 20;

                    oItem1.Height = oItem.Height;

                    oItem1.Enabled = true;

                    //SAPbouiCOM.ComboBox oComboBox = (SAPbouiCOM.ComboBox)oItem1.Specific;
                    oItem1.DisplayDesc = true;


                }

            }
            catch (Exception)
            {

                throw;
            }


        }

        public static void BindDataMatrix(this IForm form, string matrixId, string dataTableId, string sql,Dictionary<string,  Column> matrixcolums= null)
        {
            try
            {
                if (matrixId != null)
                {
                    form.Freeze(true);
                  
                   
                    DataTable dataTable = null;
                   
                    if (form.DataSources.DataTables.Count==0 || form.DataSources.DataTables.Item(dataTableId)== null)
                        dataTable = form.DataSources.DataTables.Add(dataTableId);
                    else
                        dataTable = form.DataSources.DataTables.Item(dataTableId);

                    // add a matrix to the form
                    var matrix = (Matrix)form.Items.Item(matrixId).Specific; //(Matrix)form.Items.Add(dataTableId, BoFormItemTypes.it_MATRIX).Specific;
                    //matrix.Item.Width = 500;
                    //matrix.Item.Height = 300;

                    // get some data

                    dataTable.ExecuteQuery(sql);
                   
                    if (matrixcolums!=null)
                    {
                        foreach(var col in matrixcolums)
                        {
                            matrix.Columns.Add(col.Value.UniqueID, col.Value.Type);
                           
                            matrix.Columns.Item(col.Value.UniqueID).DataBind.Bind(dataTableId, col.Key);
                        }    
                    }
                    var columns = matrix.Columns;
                    columns.Item("#").DataBind.Bind(dataTableId, dataTable.Columns.Item(0).Name);

                    for (var i = 1 ; i < dataTable.Columns.Count;i++)
                    {
                       
                        columns.Item(string.Format("Col_{0}",i-1)).DataBind.Bind(dataTableId, dataTable.Columns.Item(i).Name);
                        
                    }    
                    

                    //// create columns
                    //columns.Add("#", BoFormItemTypes.it_EDIT);
                    //columns.Add("code", BoFormItemTypes.it_EDIT);
                    //columns.Add("name", BoFormItemTypes.it_EDIT);

                    //// setup columns
                    //columns.Item("#").TitleObject.Caption = "#";
                    //columns.Item("code").TitleObject.Caption = "CardCode";
                    //columns.Item("name").TitleObject.Caption = "CardName";

                    ////bind columns to data
                    //columns.Item("code").DataBind.Bind(dataTableId, "CardCode");
                    //columns.Item("name").DataBind.Bind(dataTableId, "CardName");

                    // load the data into the rows
                    matrix.LoadFromDataSource();
                    matrix.AutoResizeColumns();

                    form.Freeze(false);

                }

            }
            catch (Exception ex)
            {

                SboApp.Application.SetStatusBarMessage(ex.Message, BoMessageTime.bmt_Short, true);
            }


        }



        /// <summary>
        /// Freeze Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public static Freeze FreezeEx(this IForm form)
        {
            return new Freeze(form);
        }
    }

}