﻿using ITCO.SboAddon.Framework.Helpers;
using System;
using System.Configuration;
using Common.Logging;

namespace ITCO.SboAddon.Framework
{
    using ITCO.SboAddon.Framework.Data;
    using ITCO.SboAddon.Framework.Data.Dapper;
    using ITCO.SboAddon.Framework.Extensions;
    using Newtonsoft.Json;
    using SAPbobsCOM;
    using SAPbouiCOM;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Dynamic;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;

    /// <summary>
    /// SBO Application Connector
    /// </summary>
    public class SboApp
    {
        private static SAPbouiCOM.Application _application;
        private static SAPbobsCOM.Company _diCompany;
        private static string Token = "";
        private static bool IsDoubleClick = false;
        /// <summary>
        /// Debug Connection String
        /// </summary>
        public const string DebugConnectionString =
            "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056";

        /// <summary>
        /// Common Logger
        /// </summary>
        internal static ILog Logger = LogManager.GetLogger<SboApp>();

        /// <summary>
        /// Set existing DI and/or UI Api Connection
        /// </summary>
        /// <param name="diCompany">SAPbobsCOM.Company</param>
        /// <param name="application">SAPbouiCOM.Application</param>
        /// <param name="loggingEnabled">Is SBO Common Logging enabled</param>
        public static void SetApiConnection(
            SAPbobsCOM.Company diCompany, SAPbouiCOM.Application application = null, bool loggingEnabled = true)
        {
            _diCompany = diCompany;

            if (application != null)
                _application = application;

            if (loggingEnabled)
                SboAppLogger.Enable();
        }

        /// <summary>
        /// Connect UI and DI API
        /// </summary>
        /// <param name="connectionString">Connection String from SBO Main Application</param>
        /// <param name="loggingEnabled">Is SBO Common Logging enabled</param>
        [Obsolete]
        public static void Connect(string connectionString = null, bool loggingEnabled = true)
        {

            // connectionString = DebugConnectionString;
            if (connectionString == null)
            {
                connectionString = Environment.GetCommandLineArgs().Length > 1 ? 
                    Convert.ToString(Environment.GetCommandLineArgs().GetValue(1)) : string.Empty;
            }

            var sboGuiApi = new SAPbouiCOM.SboGuiApi();
            //_diCompany = new SAPbobsCOM.Company();

            try
            {
                sboGuiApi.Connect(connectionString);
                _application = sboGuiApi.GetApplication();
                Token = GetToken();
                //var contextCookie = _diCompany.GetContextCookie();
                //var diCompanyConnectionString = _application.Company.GetConnectionContext(contextCookie);

                //var responseCode = _diCompany.SetSboLoginContext(diCompanyConnectionString);
                //ErrorHelper.HandleErrorWithException(responseCode, "DI API Could not Set Sbo Login Context");

                //var connectResponse = _diCompany.Connect();
                //ErrorHelper.HandleErrorWithException(connectResponse, "DI API Could not connect");

                if (loggingEnabled)
                {
                    SboAppLogger.Enable();
                }

                var assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                Logger.Info($"{assemblyName} connected");

                SetAppEvents();
            }
            catch (Exception ex)
            {
                Logger.Error($"SboApp UI Connect Error: {ex.Message}", ex);
                throw;
            }
        }

        /// <summary>
        /// Connect only DI Api
        /// </summary>
        /// <param name="serverName">SQL Server Name</param>
        /// <param name="serverType">Server type</param>
        /// <param name="companyDb"></param>
        /// <param name="dbUsername"></param>
        /// <param name="dbPassword"></param>
        /// <param name="username">SBO Username</param>
        /// <param name="password">SBO Password</param>
        /// <param name="licenceServer">Licence Server</param>
        public static void DiConnect(string serverName, SAPbobsCOM.BoDataServerTypes serverType, string companyDb,
            string dbUsername = null, string dbPassword = null, string username = null, string password = null, string licenceServer = null)
        {
            _diCompany = new SAPbobsCOM.Company
            {
                Server = serverName,
                DbServerType = serverType,
                CompanyDB = companyDb
            };
            
            if (licenceServer != null)
                _diCompany.LicenseServer = licenceServer;

            if (string.IsNullOrEmpty(username))
            {
                _diCompany.UseTrusted = true;
            }
            else
            {
                _diCompany.UseTrusted = false;
                _diCompany.UserName = username;
                _diCompany.Password = password;
                _diCompany.DbUserName = dbUsername;
                _diCompany.DbPassword = dbPassword;
            }

            var connectResponse = _diCompany.Connect();

            if (connectResponse != 0)
            {
                int errCode;
                string errMsg;
                _diCompany.GetLastError(out errCode, out errMsg);

                Logger.Debug($"Servername={serverName}, CompanyDb={companyDb}, ServerType={serverType}, " +
                             $"DbUsername={dbUsername}, DbPassword={dbPassword}, " +
                             $"SboUsername={username}, SboPassword={password}, " +
                             $"UseTrusted={_diCompany.UseTrusted}, " +
                             $"LicenceServer={licenceServer}");

                throw new Exception($"DI Connect Error: {errCode} {errMsg}");
            }

            if (serverType == BoDataServerTypes.dst_HANADB)
            {
                HanaHandlers.Register();
            }
        }
        /// <summary>
        /// Connect only DI Api from app.config
        /// </summary>
        /// <example>
        /// Sbo:ServerName
        /// Sbo:ServerType (BoDataServerTypes eg. MSSQL2012)
        /// Sbo:CompanyDb
        /// Sbo:DbUsername
        /// Sbo:DbPassword
        /// Sbo:Username
        /// Sbo:Password
        /// Sbo:LicenceService
        /// </example>
        public static void DiConnectFromAppConfig()
        {
            var serverName = ConfigurationManager.AppSettings["Sbo:ServerName"];

            var serverType = GetServiceType(ConfigurationManager.AppSettings["Sbo:ServerType"]);

            var companyDb = ConfigurationManager.AppSettings["Sbo:CompanyDb"];
            var dbUsername = ConfigurationManager.AppSettings["Sbo:DbUsername"];
            var dbPassword = ConfigurationManager.AppSettings["Sbo:DbPassword"];
            var username = ConfigurationManager.AppSettings["Sbo:Username"];
            var password = ConfigurationManager.AppSettings["Sbo:Password"];
            var licenceServer = ConfigurationManager.AppSettings["Sbo:LicenceServer"];

            DiConnect(serverName, serverType, companyDb, dbUsername, dbPassword, username, password, licenceServer);
        }

        internal static SAPbobsCOM.BoDataServerTypes GetServiceType(string serverTypeString)
        {
            SAPbobsCOM.BoDataServerTypes serverType;
            //if (serverTypeString == "MSSQL2016")
            //{
            //    serverType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
            //}
 
            if (!Enum.TryParse("dst_" + serverTypeString, out serverType))
            {
                var availableServerTypes = string.Join(",", Enum.GetNames(typeof(SAPbobsCOM.BoDataServerTypes)));
                throw new Exception($"Could not parse server type from '{serverTypeString}'. Available server types {availableServerTypes}");
            }
            return serverType;
        }

        /// <summary>
        /// SBO UI Application Object
        /// </summary>
        public static SAPbouiCOM.Application Application
        {
            get
            {
                if (!ApplicationConnected)
                    throw new Exception("SBO UI API Not Connected");

                return _application;
            }
        }

        /// <summary>
        /// SBO DI Company Object
        /// </summary>
        public static SAPbobsCOM.Company Company
        {
            get
            {
                if (!DiConnected)
                    throw new Exception("SBO DI API Not Connected");

                return _diCompany;
            }
        }

        /// <summary>
        /// Check if SBO UI API is Connected
        /// </summary>
        public static bool ApplicationConnected => _application != null;//&& DiConnected;

        /// <summary>
        /// Check if SBO DI API is Connected
        /// </summary>
        public static bool DiConnected => _diCompany != null && _diCompany.Connected;

        /// <summary>
        /// Is connection aganst HANA DB
        /// </summary>
        public static bool IsHana => Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB;

        /// <summary>
        /// Set Default App Events
        /// </summary>
        [Obsolete]
        protected static void SetAppEvents()
        {
            _application.MenuEvent += _application_MenuEvent;
            _application.ItemEvent += _application_ItemEvent;
            _application.FormDataEvent += _application_FormDataEvent;
            _application.AppEvent += Application_AppEvent;
        }

        private static void _application_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            
            //Add PO
            if (pVal.FormTypeEx == "142" && pVal.BeforeAction==false && pVal.ActionSuccess == true && pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)
            {
                try
                {
                    //string docKey = pVal.ObjectKey;
                    int dockey = pVal.GetDocEntry();
                    SboApp.Application.StatusBar.SetSystemMessage("Send mail PO " + dockey.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                    //Code send email 
                   
                    var sbo = new SboDbConnection();
                   
                    foreach (var _po in   sbo.Query<PO>(string.Format("EXEC USP_GET_PO_INFO_SENDMAIL {0}", dockey)))
                    {
                        SendEmailToCenter(_po.DocEntry, _po.DocDate, _po.DeliveryDate, _po.Remarks, _po.CardCode, _po.CardName, _po.CenterCode, _po.CenterName, _po.Email);
                    }    
                    //using (var query = new SboRecordsetQuery("EXEC USP_GET_PO_INFO_SENDMAIL {0}", dockey))
                    //{
                        
                        //foreach (var row in query.Result)
                        //{
                        //    var DocEntry = row.Item("DocEntry").Value;
                        //    var DocDate = row.Item("DocDate").Value;
                        //    var DeliveryDate = row.Item("DeliveryDate").Value;
                        //    var CardCode = row.Item("CardCode").Value;
                        //    var CardName = row.Item("CardName").Value;
                        //    var Remarks = row.Item("Remarks").Value;
                        //    var CenterCode = row.Item("CenterCode").Value;
                        //    var CenterName = row.Item("CenterName").Value;
                        //    var Email = row.Item("Email").Value;
                        //    SendEmailToCenter(DocEntry, DocDate, DeliveryDate, Remarks, CardCode, CardName, CenterCode, CenterName, Email);
                        //}
                    //}
                }
                catch (Exception ex)
                {
                    throw ex;
                    
                }
                
            }

        }

        [Obsolete]
        private static  void _application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {

            BubbleEvent = true;
            //if (pVal.FormType == 150 && pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK && pVal.BeforeAction == false && pVal.ActionSuccess == true)
            //{

            //    SboApp.Application.StatusBar.SetSystemMessage("Clicked item " + pVal.ItemUID, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
            //}
            if (pVal.FormUID == "test" && (pVal.EventType==  BoEventTypes.et_FORM_VISIBLE  ) && pVal.BeforeAction==false && pVal.ActionSuccess==true)
            {
                var activeForm = SboApp.Application.Forms.Item(pVal.FormUID);

                
                if(activeForm.Visible)
                    FormExtensions.BindDataMatrix(activeForm, "Item_4", "DT_0", "SELECT ROW_NUMBER() OVER (ORDER BY ListNum )[#], 'N' [Selected], ListNum as [Id],ListName as [PriceName] FROM opln (NOLOCK) ORDER BY ListNum");

            }
            if (pVal.FormUID == "test" && pVal.ItemUID=="btOK" && (pVal.EventType == BoEventTypes.et_CLICK) && pVal.BeforeAction == false && pVal.ActionSuccess == true)
            {
                var activeForm = SboApp.Application.Forms.Item(pVal.FormUID);
                var matrix = activeForm.GetMatrix("Item_4");
                matrix.FlushToDataSource();
                DataTable dataTable = null;
                if (activeForm.DataSources.DataTables.Count > 0 && activeForm.DataSources.DataTables.Item("DT_0") != null)
                    dataTable = activeForm.DataSources.DataTables.Item("DT_0");
                var xml = dataTable.SerializeAsXML(BoDataTableXmlSelect.dxs_DataOnly);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                var json = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.Indented);
                dynamic test = JsonConvert.DeserializeObject<ExpandoObject>(json);
                foreach(var row in test.DataTable.Rows.Row)
                {
                    if(row.Cells.Cell[1].Value=="Y")
                    {
                        foreach (var cell in row.Cells.Cell)
                        {

                            SboApp.Application.SetStatusBarMessage( cell.ColumnUid + "-" + cell.Value, BoMessageTime.bmt_Short, false);
                        }
                    }    
                      
                        
                }    
                //string filepath = AppDomain.CurrentDomain.BaseDirectory+ @"\XML\" ;
                //if (!Directory.Exists(filepath))
                //    Directory.CreateDirectory(filepath);
                //File.WriteAllText(filepath + @"\DT_0.xml", xml, Encoding.ASCII);
                //if(File.Exists(filepath + @"\DT_0.xml"))
                //{
                //    System.Data.DataTable dt = DataExtensions.SapDataTableToDotNetDataTable(filepath + @"\DT_0.xml");
                //}    
               
            }
            if (pVal.FormUID == "test" && pVal.ItemUID == "Item_4" && (pVal.EventType == BoEventTypes.et_DOUBLE_CLICK) && pVal.BeforeAction == false && pVal.ActionSuccess == true)
            {
                var activeForm = SboApp.Application.Forms.Item(pVal.FormUID);
                activeForm.Freeze(true);
                var matrix = activeForm.GetMatrix("Item_4");
                matrix.FlushToDataSource();
                DataTable dataTable = null;
                if (activeForm.DataSources.DataTables.Count > 0 && activeForm.DataSources.DataTables.Item("DT_0") != null)
                    dataTable = activeForm.DataSources.DataTables.Item("DT_0");
                if(IsDoubleClick == false)
                {
                    for (int i = 0; i <= dataTable.Rows.Count - 1; i++)
                    {
                        dataTable.SetValue("Selected", i, "Y");
                        IsDoubleClick = true;
                    }
                }    
                else
                {
                    for (int i = 0; i <= dataTable.Rows.Count - 1; i++)
                    {
                        dataTable.SetValue("Selected", i, "N");
                        IsDoubleClick = false;
                    }
                }    

                matrix.LoadFromDataSource();
                activeForm.Freeze(false);
            }


            if (pVal.FormType == 133 || pVal.FormType == 940 || pVal.FormType == 179 )//|| pVal.FormType == 392
            {


                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD && pVal.BeforeAction == false && pVal.ActionSuccess == true)
                {
                    var activeForm = SboApp.Application.Forms.GetForm(pVal.FormType.ToString(), pVal.FormTypeCount);
                    //activeForm.AddButton( "2", "btPublish", "Publish");
                    //SboApp.Application.StatusBar.SetSystemMessage("Add button Publish", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                    //FormExtensions.AddCombobox(activeForm, "btPublish", "cbType");
                    //SboApp.Application.StatusBar.SetSystemMessage("Add combobox Publish Type", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                    //string query = "select '13' Code,'AR Invoice' Name Union all Select '67' Code,'Inventory Transfer' Name";
                    //FormExtensions.AddComboBoxValue_Dapper(activeForm.GetComboBox("cbType"), query);
                    //SboApp.Application.StatusBar.SetSystemMessage("Add value combobox Publish Type", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                    activeForm.AddButtonCombo("2", "btCBType", 110);
                    //SboApp.Application.SetStatusBarMessage("Add Button Combobox Publish Type", SAPbouiCOM.BoMessageTime.bmt_Short, false);

                    string query2 = "select '0' Code,'Choose Action' Name Union all select '1' Code,'Preview Draft' Name Union all Select '2' Code,'Publish Document' Name Union all Select '3' Code,'Get PDF' Name Union all Select '4' Code,'Resend Mail' Name Union all Select '5' Code,'Cancel Document' Name";
                    ButtonCombo btcb = activeForm.Get<ButtonCombo>("btCBType");
                    FormExtensions.AddButtonComboBoxValue_Dapper(btcb, query2);
                    btcb.SelectExclusive(0, BoSearchKey.psk_Index);
                    //SboApp.Application.SetStatusBarMessage("Add value combobox Publish Type", SAPbouiCOM.BoMessageTime.bmt_Short, false);

                }
                if (pVal.ItemUID == "btCBType" && pVal.EventType == BoEventTypes.et_COMBO_SELECT && pVal.ActionSuccess == true && pVal.BeforeAction == false)
                {
                    var activeForm = SboApp.Application.Forms.GetForm(pVal.FormType.ToString(), pVal.FormTypeCount);
                    ButtonCombo btcb = activeForm.Get<ButtonCombo>("btCBType");
                    try
                    {
                        activeForm.Freeze(true);
                        if (btcb.Selected.Value != "0")
                        {
                            if (pVal.FormMode != 1)
                            {


                                btcb.SelectExclusive(0, BoSearchKey.psk_Index);
                                //SboApp.Application.StatusBar.SetSystemMessage("Must choose Docentry First!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                                SboApp.Application.SetStatusBarMessage("Must choose Docentry First!", SAPbouiCOM.BoMessageTime.bmt_Short, true);
                            }
                            else
                            {

                                string docentry = "";
                                int transtype = 0;
                                switch (pVal.FormType)
                                {
                                    case 133:
                                        docentry = activeForm.GetEditText("8").Value;
                                        transtype = 13;
                                        break;
                                    case 179:
                                        docentry = activeForm.GetEditText("8").Value;
                                        transtype = 14;
                                        break;
                                    case 940:
                                        docentry = activeForm.GetEditText("11").Value;
                                        transtype = 67;
                                        break;
                                    case 392:
                                        docentry = activeForm.GetEditText("141").Value;
                                        transtype = 30;
                                        break;
                                    default:
                                        break;
                                }
                                //SboApp.Application.SetStatusBarMessage("choose Docentry: " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                                switch (btcb.Selected.Value)
                                {
                                    case "1":
                                        SboApp.Application.SetStatusBarMessage("Please wait a moment...", SAPbouiCOM.BoMessageTime.bmt_Medium, false);

                                        PreviewDraft(docentry, transtype.ToString());
                                        break;
                                    case "2":
                                        SboApp.Application.SetStatusBarMessage("Please wait a moment...", SAPbouiCOM.BoMessageTime.bmt_Medium, false);

                                        if (transtype == 14)
                                            AdjustDocument(docentry);
                                        else
                                            PublishDocument(docentry, transtype);
                                        break;
                                    case "3":
                                        SboApp.Application.SetStatusBarMessage("Please wait a moment...", SAPbouiCOM.BoMessageTime.bmt_Medium, false);

                                        GetPDF(docentry, transtype.ToString(), SboApp.Application.Company.UserName);
                                        break;
                                    case "4":
                                        SboApp.Application.SetStatusBarMessage("Please wait a moment...", SAPbouiCOM.BoMessageTime.bmt_Medium, false);

                                        ResendMail(docentry, transtype);
                                        break;
                                    case "5":

                                        if (transtype == 13 || transtype == 14)
                                        {
                                            SboApp.Application.SetStatusBarMessage("Please wait a moment...", SAPbouiCOM.BoMessageTime.bmt_Medium, false);
                                            if(CheckDocCancelValid(docentry, transtype.ToString()))
                                                CancelDocument(docentry, transtype);
                                            else
                                            {
                                                SboApp.Application.SetStatusBarMessage("Please choose original documents!", SAPbouiCOM.BoMessageTime.bmt_Short, true);
                                                return;
                                            }
                                        }

                                        else
                                            SboApp.Application.SetStatusBarMessage("No support this document!", SAPbouiCOM.BoMessageTime.bmt_Short, true);

                                        break;
                                    default:
                                        break;
                                }


                                //try
                                //{
                                //    btcb.SelectExclusive(0, BoSearchKey.psk_Index);
                                //}
                                //catch (Exception ex) { throw ex; }

                            }

                        }

                    }
                    catch (Exception ex1)
                    {

                        activeForm.Freeze(false);
                    }
                    finally
                    {
                        activeForm.Freeze(false);
                        
                    }

                }

            }

        }

        [Obsolete]
        private static void _application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
           
            if ((pVal.MenuUID == "1290" || pVal.MenuUID == "1288" || pVal.MenuUID == "1289" || pVal.MenuUID == "1291" || pVal.MenuUID == "1304") && pVal.BeforeAction == false)
            {
                
                var activeForm = SboApp.Application.Forms.ActiveForm;
                try
                {
                    if (activeForm.TypeEx == "133" || activeForm.TypeEx == "940" || activeForm.TypeEx == "179" || activeForm.TypeEx == "392")
                    {
                        ButtonCombo btcb = activeForm.Get<ButtonCombo>("btCBType");
                        btcb.SelectExclusive(0, BoSearchKey.psk_Index);
                    }
                    
                }
                catch (Exception ex) { throw ex; }

            }

        }

        private static void Application_AppEvent(SAPbouiCOM.BoAppEventTypes eventType)
        {
            switch (eventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    Disconnect();
                    System.Windows.Forms.Application.Exit();
                    GC.Collect();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    Disconnect();
                    System.Windows.Forms.Application.Exit();
                    GC.Collect();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    Disconnect();
                    System.Windows.Forms.Application.Exit();
                    GC.Collect();
                    break;
            }
        }

        private static void Disconnect()
        {
            try
            {
                if (_diCompany.Connected)
                    _diCompany.Disconnect();
            }
            finally
            {
                _diCompany = null;
                _application = null;
            }
        }
        private static bool CheckDocCancelValid(string docentry,string transtype)
        {
            try
            {
                var sbo = new SboDbConnection();

                string tt = sbo.QueryFirstOrDefault<string>(string.Format("SELECT [dbo].[fn_SINVOICE_CHECK_CANCEL_DOC] ('{0}','{1}')", docentry, transtype));
                if (tt == "0")
                    return false;
                return true;
            }
            catch (Exception)
            {

                return false;
            }
            
        }
        private static void SendEmailToCenter(dynamic docentry, dynamic docdate, dynamic docduedate, dynamic remarks, dynamic cardcode, dynamic cardname, dynamic centercode, dynamic centername, dynamic mailto)
        {
            
            using (var client = new HttpClient())
            {

                dynamic p = new ExpandoObject();

                p.to = mailto;
                p.cc = "trai.tran@nkidgroup.com;tai.nguyenthanh@nkidgroup.com";
                p.subject = string.Format( "Đơn đặt hàng NCC: {0} - {1}",cardcode,cardname);

                //p.FromAddress = "tech@nkidcorp.com";
                //p.ToAddress = mailto;
                //p.CCAddress = "";
                //p.BCCAddress = "";
                //p.Subject = "Thông báo đơn đặt hàng NCC";

                string _body = "<p>Dear Anh/chị,</p>";
                _body += "<p>Phòng <b>Procurement</b> vừa lên đơn đặt hàng với nhà cung cấp: <b>" + cardname + "</b></p>";
                _body += "<p>Số PO: <b>" + docentry + "</b></p>";
                _body += "<p>Ngày dự kiến giao hàng: <b>" + docduedate + "</b></p>";
                _body += "<p>Nội dung ghi chú: <b>" + remarks + "</b></p>";
                _body += "<p>Anh/chị vui lòng tiếp nhận thông tin và tiến hành các bước tiếp theo." + "</p>";

                //p.Body = _body;
                p.body = _body;
                //client.BaseAddress = new Uri("http://api-edi.nkidgroup.com:5555/");
                client.BaseAddress = new Uri("http://115.79.54.253:4444/");
                client.Timeout = new TimeSpan(0, 15, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);
                var content = new StringContent(JsonConvert.SerializeObject(p), Encoding.UTF8, "application/json");
                //var response = client.PostAsync("v1/Mail/SendMail", content).Result;
                var response = client.PostAsync("Mailer/Send", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    SboApp.Application.StatusBar.SetSystemMessage("Send mail to " + mailto + " success", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                    Console.Write("Success");
                }
                else
                {
                    SboApp.Application.StatusBar.SetSystemMessage("Error mail to " + mailto + response.StatusCode, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                    Console.Write("Error");
                }

            }
        }
        private static string GetToken()
        {
            using (var client = new HttpClient())
            {
                dynamic p = new ExpandoObject();
                p.UserName = "NKID";
                p.PassWord = "NKID@SAP";

                //client.BaseAddress = new Uri("http://api-edi.nkidgroup.com:5555/");
                client.BaseAddress = new Uri("http://115.79.54.253:4444/");
                client.Timeout = new TimeSpan(0, 15, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var content = new StringContent(JsonConvert.SerializeObject(p), Encoding.UTF8, "application/json");
                //var response = client.PostAsync("v1/Token", content).Result;
                var response =  client.PostAsync("api/Token", content).Result;
                if (response.IsSuccessStatusCode)
                    return JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
                else
                    return "";

            }
        }
        private static async void GetPDF(string docentry,string type,string exchangeuser)
        {
            using (var client = new HttpClient())
            {

               
                //client.BaseAddress = new Uri("http://api-edi.nkidgroup.com:5555/");
                client.BaseAddress = new Uri("http://115.79.54.253:4444/");
                client.Timeout = new TimeSpan(0, 15, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);

                //var response = client.PostAsync("v1/Mail/SendMail", content).Result;
                var response = await client.GetAsync(string.Format( "api/GetPDF?DocEntry={0}&transType={1}&exchangeuser={2}", docentry,type, exchangeuser));
                if(response.IsSuccessStatusCode)
                {
                    if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"/pdf/" + type + "/"))
                        Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"/pdf/" + type + "/");

                    string path = AppDomain.CurrentDomain.BaseDirectory + @"/pdf/" + type + "/" + docentry + ".pdf";
                    try
                    {
                        if (File.Exists(path))
                        {
                            File.Delete(path);
                        }
                    }
                    catch (Exception)
                    {
                        SboApp.Application.SetStatusBarMessage("Please close pdf file: " + docentry + ".pdf", SAPbouiCOM.BoMessageTime.bmt_Short, true);

                        return;
                    }

                    using (var stream = await response.Content.ReadAsStreamAsync())
                    {
                        using (var fileStream = new FileStream(path, mode: FileMode.Create, access: FileAccess.Write, share: FileShare.None))
                        {
                            await stream.CopyToAsync(fileStream);
                        }
                    }
                    if (File.Exists(path))
                    {
                        var p = new Process();
                        p.StartInfo = new ProcessStartInfo(path)
                        {
                            UseShellExecute = true
                        };
                        p.Start();
                    }
                    SboApp.Application.SetStatusBarMessage("The processing is complete", SAPbouiCOM.BoMessageTime.bmt_Short, false);

                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        SboApp.Application.SetStatusBarMessage("Can't get pdf document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                        return;
                    }
                    else
                    {
                        SboApp.Application.SetStatusBarMessage(string.Format("Document {0} is not publish", docentry), SAPbouiCOM.BoMessageTime.bmt_Short, true);

                        return;
                    }
                    
                }    
                


            }
        }
        private static async void PreviewDraft(string docentry, string type)
        {
            using (var client = new HttpClient())
            {


                //client.BaseAddress = new Uri("http://api-edi.nkidgroup.com:5555/");
                client.BaseAddress = new Uri("http://115.79.54.253:4444/");
                client.Timeout = new TimeSpan(0, 15, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);

                //var response = client.PostAsync("v1/Mail/SendMail", content).Result;
                var response = await client.GetAsync(string.Format("api/PreviewDraft?DocEntry={0}&transType={1}", docentry, type));
                if(response.IsSuccessStatusCode)
                {
                    if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"/pdf/" + type + "/"))
                        Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"/pdf/" + type + "/");

                    string path = AppDomain.CurrentDomain.BaseDirectory + @"/pdf/" + type + "/Draft_" + docentry + ".pdf";
                    try
                    {
                        if (File.Exists(path))
                        {
                            File.Delete(path);
                        }
                    }
                    catch (Exception)
                    {
                        SboApp.Application.SetStatusBarMessage("Please close pdf file: Draft_" + docentry + ".pdf", SAPbouiCOM.BoMessageTime.bmt_Short, true);

                        return;
                    }
                    using (var stream = await response.Content.ReadAsStreamAsync())
                    {


                        using (var fileStream = new FileStream(path, mode: FileMode.Create, access: FileAccess.Write, share: FileShare.None))
                        {
                            await stream.CopyToAsync(fileStream);
                        }

                    }
                    if (File.Exists(path))
                    {
                        var p = new Process();
                        p.StartInfo = new ProcessStartInfo(path)
                        {
                            UseShellExecute = true
                        };
                        p.Start();
                    }
                    SboApp.Application.SetStatusBarMessage("The processing is complete", SAPbouiCOM.BoMessageTime.bmt_Short, false);

                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        SboApp.Application.SetStatusBarMessage("Can't preview draft document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                        return;
                    }
                    else
                    {
                        SboApp.Application.SetStatusBarMessage(string.Format("Document {0} can't preview draft", docentry), SAPbouiCOM.BoMessageTime.bmt_Short, true);

                        return;
                    }    
                   
                }
                
               

            }
        }
        private static async void PublishDocument(string docentry, int type)
        {
            using (var client = new HttpClient())
            {


                dynamic doc = new ExpandoObject();
                doc.DocNo = docentry;
                doc.DocType = type;
                //client.BaseAddress = new Uri("http://api-edi.nkidgroup.com:5555/");
                client.BaseAddress = new Uri("http://115.79.54.253:4444/");
                client.Timeout = new TimeSpan(0, 15, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);

                var content = new StringContent(JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.Indented), Encoding.UTF8, "application/json");

                //var response = client.PostAsync("v1/Mail/SendMail", content).Result;
                var response = await client.PostAsync("api/PublishDocument", content);

                if (response.IsSuccessStatusCode)
                {
                    SboApp.Application.ActivateMenuItem("1304");
                    SboApp.Application.SetStatusBarMessage("Success publish document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, false);

                }
                else
                {
                    if(response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        SboApp.Application.SetStatusBarMessage("Can't publish document " + docentry , SAPbouiCOM.BoMessageTime.bmt_Short, true);

                    }
                    else
                    {
                        dynamic output = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        SboApp.Application.SetStatusBarMessage("Error publish document " + docentry + " - " + output.description, SAPbouiCOM.BoMessageTime.bmt_Short, true);

                    }

                }
            }
        }
        private static async void CancelDocument(string docentry, int type)
        {
            using (var client = new HttpClient())
            {

                dynamic doc = new ExpandoObject();
                doc.DocNo = docentry;
                doc.DocType = type;
                //client.BaseAddress = new Uri("http://api-edi.nkidgroup.com:5555/");
                client.BaseAddress = new Uri("http://115.79.54.253:4444/");
                client.Timeout = new TimeSpan(0, 15, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);

                var content = new StringContent(JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.Indented), Encoding.UTF8, "application/json");

                //var response = client.PostAsync("v1/Mail/SendMail", content).Result;
                var response = await client.PostAsync("api/CancelDocument", content);
                if (response.IsSuccessStatusCode)
                {
                    SboApp.Application.ActivateMenuItem("1304");
                    SboApp.Application.SetStatusBarMessage("Success cancel document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, false);

                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        SboApp.Application.SetStatusBarMessage("Can't cancel document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, true);

                    }
                    else
                    {
                        dynamic output = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        SboApp.Application.SetStatusBarMessage(output.description, SAPbouiCOM.BoMessageTime.bmt_Short, true);

                    }

                }
            }
        }
        private static async void AdjustDocument(string docentry)
        {
            using (var client = new HttpClient())
            {

                dynamic doc = new ExpandoObject();
                doc.DocNo = docentry;
                doc.DocType = 14;

                //client.BaseAddress = new Uri("http://api-edi.nkidgroup.com:5555/");
                client.BaseAddress = new Uri("http://115.79.54.253:4444/");
                client.Timeout = new TimeSpan(0, 15, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);
                var content = new StringContent(JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.Indented), Encoding.UTF8, "application/json");


                //var response = client.PostAsync("v1/Mail/SendMail", content).Result;
                var response = await client.PostAsync("api/AdjustDocument", content);
                if (response.IsSuccessStatusCode)
                {
                    SboApp.Application.ActivateMenuItem("1304");
                    SboApp.Application.SetStatusBarMessage("Success publish document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, false);

                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        SboApp.Application.SetStatusBarMessage("Can't publish document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, true);

                    }
                    else
                    {
                        dynamic output = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        SboApp.Application.SetStatusBarMessage(output.description, SAPbouiCOM.BoMessageTime.bmt_Short, true);

                    }

                }
            }
        }
        private static async void ResendMail(string docentry, int type)
        {
            using (var client = new HttpClient())
            {


                dynamic doc = new ExpandoObject();
                doc.DocNo = docentry;
                doc.DocType = type;
                //client.BaseAddress = new Uri("http://api-edi.nkidgroup.com:5555/");
                client.BaseAddress = new Uri("http://115.79.54.253:4444/");
                client.Timeout = new TimeSpan(0, 15, 0);
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);

                var content = new StringContent(JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.Indented), Encoding.UTF8, "application/json");

                //var response = client.PostAsync("v1/Mail/SendMail", content).Result;
                var response = await client.PostAsync("api/ResendMail", content);

                if (response.IsSuccessStatusCode)
                {
                    SboApp.Application.ActivateMenuItem("1304");
                    SboApp.Application.SetStatusBarMessage("Success Resend Email Document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, false);

                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        SboApp.Application.SetStatusBarMessage("Can't Resend Email Document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                       
                    }
                    else
                    {
                        dynamic output = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        SboApp.Application.SetStatusBarMessage("Can't Resend Email Document " + docentry, SAPbouiCOM.BoMessageTime.bmt_Short, true);

                    }

                }
            }
        }
    }
    public class PO
    {
       
        public int DocEntry { get; set; }
        public dynamic DocDate { get; set; }
        public dynamic DeliveryDate { get; set; }
        public dynamic CardCode { get; set; }
        public dynamic CardName { get; set; }
        public dynamic Remarks { get; set; }
        public dynamic CenterCode { get; set; }
        public dynamic CenterName { get; set; }
        public dynamic Email { get; set; }
    }
}