﻿using ITCO.SboAddon.Framework.Constants;
using ITCO.SboAddon.Framework.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVTAddon
{
    public class TestController : FormController, IFormMenuItem
    {
        public string MenuItemTitle => "Test Form";
        public string ParentMenuItemId => SboMenuItem.Sales;
    }
}
